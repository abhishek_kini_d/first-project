import time
import string

fname= input("Enter file name:")        #input the filename

starttime=time.time()

f_open = open(fname,'r')                #opening the file
data = f_open.read()                    #reading contents of given file and storing in a variable

#print("The file contains the following content:\n")     
#print(data)                             #printing the read data from file

num_words=0                             #initializing total number of words count to zero
counts = dict()                         #declaring a dictionary for holding count values of each word
words = data.translate(data.maketrans("","",string.punctuation)).split()              #Replacing the punctuations with empty space and splitting the file data into words
endtime=time.time()


num_words += len(words)                 #calculating total number of words in file
print("\n Number of words:",num_words)  #printing total number of words

data_nospace=data.translate({ord(c): None for c in string.whitespace}) #removal of white spaces throughout the file
numb_char =len(data_nospace)        #calculation of total characters 

exec_time=endtime-starttime
print("\n File Parsing time:",exec_time)

print("\n Number of characters(no space):",numb_char)
print("\n Number of characters(with space):",len(data))


#for word in words:
    #if word in counts:                  #if a reapeated word re-appears 
       # counts[word] += 1               #increment count value of that particular word by +1
   # else:                               #if it is a new unique word
       # counts[word] = 1                #intialize the count value of new word to 1

#print("-"*20)
#print("Words\t      |Times repeated")
#print("-"*20)
#for  (word,occurance)  in counts.items():   #printing the unique words found in file along with their occurances
    #if occurance>1:
        #
        #print('{:15}{:3}'.format(word,occurance))
f_open.close()
